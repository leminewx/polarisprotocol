package main

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitee.com/leminewx/polarisprotocol/v1/connection"
	"gitee.com/leminewx/polarisprotocol/v1/model"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

func main() {
	conn, err := net.Dial("tcp", ":20771")
	if err != nil {
		panic(err)
	}

	// 认证
	if err := connection.RequestAuth(conn, protocol.IDE_INSTANCE, "123qwe!@#"); err != nil {
		panic(err)
	}

	c := connection.NewInstanceConn(conn)
	c.RemoteIdentity = protocol.IDE_REGISTRAR
	go func() {
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, syscall.SIGINT) // 只接收INT信号（Ctrl+C）

		<-sigChan
		fmt.Println("Shutdowning...")
		c.Unregister()
		os.Exit(1)
	}()

	if err := c.Register(&model.Instance{
		Id:          "123qwe123",
		Addr:        "10.255.40.112:8879",
		Name:        "test",
		Protocol:    model.INS_PROTO_HTTP,
		Namespace:   "test",
		ServiceName: "user",
		ServiceVer:  "v4",
	}); err != nil {
		conn.Close()
		panic(err)
	}
	time.Sleep(10 * time.Second)
	c.Unregister()
}
