package main

import (
	"flag"
	"log/slog"
	"os"
	"os/signal"
	"strings"

	"syscall"

	"gitee.com/leminewx/polarisprotocol"
	"gitee.com/leminewx/polarisprotocol/v1/client"
	"gitee.com/leminewx/polarisprotocol/v1/connection"
	"gitee.com/leminewx/polarisprotocol/v1/model"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

func main() {
	localAddr := flag.String("host", ":20771", "设置本地地址")
	pwd := flag.String("pwd", polarisprotocol.DefaultPassword, "注册中心集群的接入密码")
	remoteAddr := flag.String("node", "", "指定一个注册中心集群的节点，通过这个节点将本注册中心接入集群")
	flag.Parse()

	reg := client.NewRegistrarCenter(*localAddr)
	reg.WithPassword(*pwd)
	if *remoteAddr != "" {
		reg.RegisterToRegistrar(*remoteAddr, *pwd)
	}

	go func() {
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, syscall.SIGINT) // 只接收INT信号（Ctrl+C）
		<-sigChan
		reg.Close()
	}()

	reg.WithCommandHandlers(map[protocol.Command]connection.CmdHandler{
		// 注册
		protocol.CMD_REGISTER: func(p *protocol.Procotol, c connection.Conn) {
			switch p.Identity {
			case protocol.IDE_GATEWAY:
				// 同步本端数据到新注册中心
				if err := c.Send(protocol.CMD_SYNC_REG, reg.Dataset); err != nil {
					slog.Error("parse data failed: "+err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					return // TODO: 上报告警
				}

				// 注册并广播
				reg.Dataset.RegisterGateway(c.GetCid())
				reg.BroadcastRegistrars("", protocol.CMD_BORADCAST_REGISTER_GATE, nil)
			case protocol.IDE_INSTANCE:
				// 解析数据体
				ins := &model.Instance{}
				if err := p.ParseBody(&ins); err != nil {
					slog.Error("parse data failed: "+err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					return // TODO: 上报告警
				}

				if err := reg.Dataset.RegisterInstance(c.GetCid(), ins); err != nil {
					slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					c.Send(protocol.CMD_FAIL, map[string]string{"msg": err.Error()})
					return // TODO: 上报告警
				}

				data := &model.RegistrarInstance{}
				data.Instance = ins
				data.Rcid = c.GetCid()
				reg.BroadcastGateways("", protocol.CMD_BORADCAST_REGISTER_INS, data)
				reg.BroadcastRegistrars("", protocol.CMD_BORADCAST_REGISTER_INS, data)
			case protocol.IDE_REGISTRAR:
				// 解析数据体
				regAddr := map[string]string{}
				if err := p.ParseBody(&regAddr); err != nil {
					slog.Error("parse data failed: "+err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					return // TODO: 上报告警
				}

				// 同步本端数据到新注册中心
				if err := c.Send(protocol.CMD_SYNC_REG, reg.Dataset); err != nil {
					slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					return // TODO: 上报告警
				}

				// 注册并广播
				reg.Dataset.SyncRegistrar(c.GetCid(), model.NewOtherRegistrar())
				reg.BroadcastRegistrars(c.GetCid(), protocol.CMD_BORADCAST_REGISTER_REG, regAddr)
			default:
				slog.Error("unknown identity", "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}
		},

		// 注销
		protocol.CMD_UNREGISTER: func(p *protocol.Procotol, c connection.Conn) {
			switch p.Identity {
			case protocol.IDE_GATEWAY: // 注销网关平台
				if _, err := reg.Dataset.UnregisterGateway(c.GetCid()); err != nil {
					slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					return // TODO: 上报告警
				}

				data := map[string]string{
					"identity": p.Identity.String(),
					"rcid":     strings.Split(c.GetLocalAddr(), ":")[0],
					"gcid":     c.GetCid(),
				}

				reg.BroadcastGateways(c.GetCid(), protocol.CMD_BORADCAST_UNREGISTER_GATE, data)
				reg.BroadcastRegistrars("", protocol.CMD_BORADCAST_UNREGISTER_GATE, data)
				reg.DeleteConnData(c)
				reg.DeleteConn(c)
			case protocol.IDE_INSTANCE: // 注销应用实例
				ins, err := reg.Dataset.UnregisterInstance(c.GetCid())
				if err != nil {
					slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					return // TODO: 上报告警
				}

				data := map[string]string{
					"identity":   p.Identity.String(),
					"rcid":       strings.Split(c.GetLocalAddr(), ":")[0],
					"icid":       c.GetCid(),
					"instanceid": ins.Id,
				}

				reg.BroadcastRegistrars("", protocol.CMD_BORADCAST_REGISTER_INS, data)
				reg.BroadcastGateways("", protocol.CMD_BORADCAST_REGISTER_INS, data)
				reg.DeleteConnData(c)
				reg.DeleteConn(c)
			case protocol.IDE_REGISTRAR:
				if err := reg.Dataset.UnregisterRegistrar(c.GetCid()); err != nil {
					slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
					return // TODO: 上报告警
				}

				data := map[string]string{
					"identity": p.Identity.String(),
					"rcid":     strings.Split(c.GetLocalAddr(), ":")[0],
				}

				reg.BroadcastGateways("", protocol.CMD_BORADCAST_REGISTER_REG, data)
				reg.BroadcastInstances("", protocol.CMD_BORADCAST_REGISTER_REG, data)
				reg.DeleteConnData(c)
				reg.DeleteConn(c)
			default:
				slog.Error("unknown identity", "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}
		},

		// 注册中心广播实例注册
		protocol.CMD_BORADCAST_REGISTER_INS: func(p *protocol.Procotol, c connection.Conn) {
			if p.Identity != protocol.IDE_REGISTRAR {
				slog.Error("not registrar, not unauthorized broadcasting", "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}

			// 解析数据体
			ins := &model.RegistrarInstance{}
			if err := p.ParseBody(&ins); err != nil {
				slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}

			icid := strings.Split(ins.Addr, ":")[0]
			if err := reg.Dataset.RegisterRegistrarInstance(c.GetCid(), icid, ins.Instance); err != nil {
				slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}

			reg.BroadcastGateways("", protocol.CMD_BORADCAST_REGISTER_INS, ins)
		},

		// 注册中心广播实例注销
		protocol.CMD_BORADCAST_UNREGISTER_INS: func(p *protocol.Procotol, c connection.Conn) {
			if p.Identity != protocol.IDE_REGISTRAR {
				slog.Error("not registrar, not unauthorized broadcasting", "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}

			// 解析数据体
			data := map[string]string{}
			if err := p.ParseBody(&data); err != nil {
				slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}

			if err := reg.Dataset.UnregisterRegistrarInstance(c.GetCid(), data["icid"]); err != nil {
				slog.Error(err.Error(), "remote", c.GetRemoteIdentity().String(), "cid", c.GetCid())
				return // TODO: 上报告警
			}

			reg.BroadcastGateways("", protocol.CMD_BORADCAST_UNREGISTER_INS, data)
		},
	})
	reg.ListenAndRun()
}
