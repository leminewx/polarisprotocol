package client

import (
	"log/slog"
	"net"
	"os"
	"strings"
	"sync"

	"gitee.com/leminewx/polarisprotocol/v1/client/listener"
	"gitee.com/leminewx/polarisprotocol/v1/connection"
	"gitee.com/leminewx/polarisprotocol/v1/model"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

// func init() {
// 	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, nil)))
// }

type RegistrarCenter struct {
	*listener.Listener

	GatewayConns   map[string]*connection.RegistrarConn // 网关平台连接
	InstanceConns  map[string]*connection.RegistrarConn // 应用实例连接
	RegistrarConns map[string]*connection.RegistrarConn // 注册中心连接

	Dataset *model.Registrar // 数据中心
	lock    sync.RWMutex
}

func NewRegistrarCenter(host string) *RegistrarCenter {
	reg := &RegistrarCenter{
		Dataset:        model.NewRegistrar(),
		GatewayConns:   make(map[string]*connection.RegistrarConn),
		InstanceConns:  make(map[string]*connection.RegistrarConn),
		RegistrarConns: make(map[string]*connection.RegistrarConn),
	}
	reg.init(host)
	return reg
}

func (own *RegistrarCenter) init(host string) {
	own.Listener = listener.NewListener(host, own.process)
	own.CloseHandler = own.BroadcastAndCloseListener
}

func (own *RegistrarCenter) RegisterToRegistrar(node string, pwd string) {
	conn, err := net.Dial("tcp", node)
	if err != nil {
		slog.Error("connected to registrar failed: "+err.Error(), "remote", "registrar", "cid", strings.Split(conn.RemoteAddr().String(), ":")[0])
		os.Exit(1)
	}

	if err := connection.RequestAuth(conn, protocol.IDE_REGISTRAR, pwd); err != nil {
		slog.Error("auth registrar failed: "+err.Error(), "remote", "registrar", "cid", strings.Split(conn.RemoteAddr().String(), ":")[0])
		os.Exit(1)
	}

	_conn := connection.NewRegistrarConn(conn, protocol.IDE_REGISTRAR, true)
	if err := _conn.Send(protocol.CMD_REGISTER, map[string]string{"addr": _conn.LocalAddr}); err != nil {
		slog.Error("auth registrar failed: "+err.Error(), "remote", "registrar", "cid", strings.Split(conn.RemoteAddr().String(), ":")[0])
		os.Exit(1)
	}

	cid := strings.Split(conn.RemoteAddr().String(), ":")[0]
	own.RegistrarConns[cid] = _conn
	go own.handle(_conn)
}
