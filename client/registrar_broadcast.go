package client

import (
	"fmt"
	"log/slog"

	"gitee.com/leminewx/polarisprotocol/v1/connection"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

// BroadcastRegisterInstance 广播注册实例
// func (own *RegistrarCenter) BroadcastRegisterInstance(ins *model.Instance) {
// 	own.BroadcastGateways(protocol.CMD_BORADCAST_REGISTER_INS, protocol.DT_JSON, ins)
// 	own.BroadcastRegistrars(protocol.CMD_BORADCAST_REGISTER_INS, protocol.DT_JSON, ins)
// }

// // BroadcastUnregisterInstance 广播注销实例
// func (own *RegistrarCenter) BroadcastUnregisterInstance(conn connection.Connection) {
// 	own.BroadcastGateways(protocol.CMD_BORADCAST_UNREGISTER_INS, protocol.DT_STRING, conn.GetCid())
// 	own.BroadcastRegistrars(protocol.CMD_BORADCAST_UNREGISTER_INS, protocol.DT_STRING, conn.GetCid())
// }

// BroadcastAll  广播到所有连接
func (own *RegistrarCenter) BroadcastAll(cid string, cmd protocol.Command, data any) {
	own.BroadcastGateways(cid, cmd, data)
	own.BroadcastInstances(cid, cmd, data)
	own.BroadcastRegistrars(cid, cmd, data)
}

// BroadcastGateways  广播到所有网关平台
func (own *RegistrarCenter) BroadcastGateways(cid string, cmd protocol.Command, data any) {
	for _, conn := range own.GatewayConns {
		if conn.GetCid() == cid {
			continue
		}

		if err := conn.Send(cmd, data); err != nil {
			slog.Error(fmt.Sprintf("%v failed: %v", cmd, err), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		} else {
			slog.Info(fmt.Sprintf("%v success", cmd), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		}
	}
}

// BroadcastInstances 广播到所有应用实例
func (own *RegistrarCenter) BroadcastInstances(cid string, cmd protocol.Command, data any) {
	for _, conn := range own.InstanceConns {
		if conn.GetCid() == cid {
			continue
		}

		if err := conn.Send(cmd, data); err != nil {
			slog.Error(fmt.Sprintf("%v failed: %v", cmd, err), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		} else {
			slog.Info(fmt.Sprintf("%v success", cmd), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		}
	}
}

// BroadcastRegistrars 广播到所有注册中心
func (own *RegistrarCenter) BroadcastRegistrars(cid string, cmd protocol.Command, data any) {
	for _, conn := range own.RegistrarConns {
		if conn.GetCid() == cid {
			continue
		}

		if err := conn.Send(cmd, data); err != nil {
			slog.Error(fmt.Sprintf("%v failed: %v", cmd, err), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		} else {
			slog.Info(fmt.Sprintf("%v success", cmd), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		}
	}
}

// BroadcastAndCloseListener 广播并关闭当前 Listener
func (own *RegistrarCenter) BroadcastAndCloseListener() {
	for _, conn := range own.GatewayConns {
		delete(own.GatewayConns, conn.GetCid())
		own.unregister(conn)
	}

	for _, conn := range own.InstanceConns {
		delete(own.InstanceConns, conn.GetCid())
		own.unregister(conn)
	}

	for _, conn := range own.RegistrarConns {
		delete(own.RegistrarConns, conn.GetCid())
		own.unregister(conn)
	}
}

// unregister 注销连接
func (own *RegistrarCenter) unregister(conn connection.Conn) {
	if err := conn.Unregister(); err != nil {
		slog.Error("unregistered connection failed: "+err.Error(), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
	} else {
		slog.Info("unregistered connection success", "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
	}
}
