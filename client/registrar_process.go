package client

import (
	"fmt"
	"log/slog"
	"net"
	"strings"

	"gitee.com/leminewx/polarisprotocol/v1/connection"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

func (own *RegistrarCenter) process(proto *protocol.Procotol, conn net.Conn) {
	cid := strings.Split(conn.RemoteAddr().String(), ":")[0]
	_conn := connection.NewRegistrarConn(conn, proto.Identity, false)

	switch proto.Identity {
	case protocol.IDE_GATEWAY:
		own.GatewayConns[cid] = _conn
	case protocol.IDE_INSTANCE:
		own.InstanceConns[cid] = _conn
	case protocol.IDE_REGISTRAR:
		own.RegistrarConns[cid] = _conn
	default:
		slog.Error(fmt.Sprintf("invalid identity: %v.", proto.Identity), "remote", proto.Identity.String(), "cid", conn.RemoteAddr().String())
		return
	}

	own.handle(_conn)
}

func (own *RegistrarCenter) handle(conn connection.Conn) {
	var err error
	// var retries int = 3
	var proto *protocol.Procotol
	for {
		if proto, err = conn.Recv(); err != nil {
			// 连接已关闭
			if conn.IsClosed() {
				break
			}

			// 异常断开重试
			// if retries > 0 {
			// 	slog.Warn("interrupt the current connection: "+err.Error(), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
			// 	retries--
			// 	continue
			// }

			// 异常中断连接，需要广播通知
			own.interrupt(proto, conn)
			return
		}
		slog.Info(proto.String())

		// 正常处理逻辑
		if handler, ok := own.CommandHandlers[proto.Command]; ok {
			handler(proto, conn)
		}

		// 注销连接
		if proto.Command == protocol.CMD_UNREGISTER {
			return
		}
	}
}

// deleteConn 删除连接
func (own *RegistrarCenter) DeleteConn(conn connection.Conn) {
	switch conn.GetRemoteIdentity() {
	case protocol.IDE_GATEWAY:
		delete(own.GatewayConns, conn.GetCid())
	case protocol.IDE_INSTANCE:
		delete(own.InstanceConns, conn.GetCid())
	default:
		delete(own.RegistrarConns, conn.GetCid())
	}
}

// deleteData 删除数据
func (own *RegistrarCenter) DeleteConnData(conn connection.Conn) {
	switch conn.GetRemoteIdentity() {
	case protocol.IDE_GATEWAY:
		if _, err := own.Dataset.UnregisterGateway(conn.GetCid()); err != nil {
			slog.Error(err.Error())
		}
	case protocol.IDE_INSTANCE:
		if _, err := own.Dataset.UnregisterInstance(conn.GetCid()); err != nil {
			slog.Error(err.Error())
		}
	case protocol.IDE_REGISTRAR:
		if err := own.Dataset.UnregisterRegistrar(conn.GetCid()); err != nil {
			slog.Error(err.Error())
		}
	}
}

func (own *RegistrarCenter) interrupt(proto *protocol.Procotol, conn connection.Conn) {
	data := map[string]string{
		"msg":      "connection interrupted",
		"identity": conn.GetRemoteIdentity().String(),
	}

	var err error
	switch proto.Identity {
	case protocol.IDE_GATEWAY:
		if data["gateAddr"], err = own.Dataset.UnregisterGateway(conn.GetCid()); err != nil {
			slog.Error(err.Error(), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		}
	case protocol.IDE_INSTANCE:
		data["rcid"] = strings.Split(conn.GetLocalAddr(), ":")[0]
		data["icid"] = conn.GetCid()

		ins, err := own.Dataset.UnregisterInstance(conn.GetCid())
		if err != nil {
			slog.Error(err.Error(), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		}
		data["instanceid"] = ins.Id
	case protocol.IDE_REGISTRAR:
		data["rcid"] = conn.GetCid()
		if err := own.Dataset.UnregisterRegistrar(conn.GetCid()); err != nil {
			slog.Error(err.Error(), "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
		}
	}

	own.BroadcastAll(conn.GetCid(), protocol.CMD_FAIL, data)
	slog.Error("abortive disconnect", "remote", conn.GetRemoteIdentity().String(), "cid", conn.GetCid())
}
