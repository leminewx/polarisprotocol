package listener

import (
	"log/slog"
	"net"
	"sync"

	"gitee.com/leminewx/polarisprotocol/v1/connection"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

// tester -t 0 -T 100 -a 3625 -l 1200 -f 999999999 -d
type ProcessHandler func(proto *protocol.Procotol, conn net.Conn)

type Listener struct {
	host     string // 地址
	pwd      string // 密码
	isClosed bool   // 关闭标志位
	listener net.Listener

	CloseHandler    func()                                         // Listener 关闭处理器，主要用于广播通知
	ProcessHandler  ProcessHandler                                 // Listener 连接处理器
	CommandHandlers map[protocol.Command]connection.CommandHandler // Listener 指令处理器

	lock sync.RWMutex
}

func NewListener(host string) *Listener {
	return &Listener{
		host:            host,
		CommandHandlers: make(map[protocol.Command]connection.CommandHandler),
	}
}

func (own *Listener) WithPassword(pwd string) *Listener {
	own.pwd = pwd
	return own
}

func (own *Listener) WithCloseHandler(handler func()) *Listener {
	own.CloseHandler = handler
	return own
}

func (own *Listener) WithProcessHandler(handler ProcessHandler) *Listener {
	own.ProcessHandler = handler
	return own
}

func (own *Listener) WithCommandHandlers(handlers map[protocol.Command]connection.CommandHandler) *Listener {
	for cmd, handler := range handlers {
		own.CommandHandlers[cmd] = handler
	}
	return own
}

func (own *Listener) ListenAndRun() {
	var err error
	if own.listener, err = net.Listen("tcp", own.host); err != nil {
		panic(err)
	}
	slog.Info("listener is listening...")

	for {
		conn, err := own.listener.Accept()
		if err != nil {
			// 如果listener已经关闭，则直接退出
			if own.isClosed {
				slog.Info("listener is closed")
			} else {
				slog.Error("listener is error: " + err.Error())
			}
			break
		}

		// 关闭时，不再接受连接
		if own.isClosed {
			protocol.Write(conn, protocol.CMD_FAIL, protocol.IDE_REGISTRAR, conn.LocalAddr().String(), "registrar is closed")
			conn.Close()
			continue
		}

		go func() {
			// 认证连接
			proto, err := connection.ResponseAuth(conn, protocol.IDE_REGISTRAR, own.pwd)
			if err != nil {
				conn.Close()
				slog.Error("auth failed: "+err.Error(), "identity", proto.Identity.String(), "addr", conn.Meta.RemoteAddr)
				return
			}

			// 处理连接
			own.ProcessHandler(proto, conn)
		}()
	}
}

func (own *Listener) Close() {
	if !own.closed {
		own.closed = true
		slog.Info("listener will be closed")
		if own.CloseHandler != nil {
			own.CloseHandler()
		}
		own.listener.Close()
	}
}

func (own *Listener) IsClosed() bool {
	return own.closed
}
