package gateway

import (
	"fmt"
	"log/slog"
	"net"

	"gitee.com/leminewx/polarisprotocol/v1/connection"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

type GatewayClient struct {
	pwd      string
	beatItv  int
	regAddrs map[string]struct{}
	conn     *connection.GatewayConn
	handlers map[protocol.Command]connection.CommandHandler
}

func NewGatewayClient(regAddr, pwd string, heartbeatInterval int) (*GatewayClient, error) {
	gate := &GatewayClient{
		pwd:      pwd,
		beatItv:  heartbeatInterval,
		regAddrs: make(map[string]struct{}),
		handlers: make(map[protocol.Command]connection.CommandHandler),
	}
	return gate, gate.init(regAddr, gate.beatItv)
}

func (own *GatewayClient) init(regAddr string, heartbeatInterval int) error {
	conn, err := net.Dial("tcp", regAddr)
	if err != nil {
		return fmt.Errorf("registered to registrar failed: %v", err)
	}

	own.conn = connection.NewGatewayConn(conn, heartbeatInterval)
	return own.conn.Auth(own.pwd)
}

func (own *GatewayClient) WithCommandHandlers(handlers map[protocol.Command]connection.CommandHandler) {
	for k, v := range handlers {
		own.handlers[k] = v
	}
}

func (own *GatewayClient) ListenAndRun() {
	for addr := range own.regAddrs {
		// 初始化连接
		if err := own.init(addr, own.beatItv); err != nil {
			slog.Error("gateway client init failed: "+err.Error(), "identity", own.conn.Meta.RemoteIdentity.String(), "addr", addr)
			continue
		}

		// 阻塞监听
		for {
			// 接收数据
			proto, err := own.conn.Recv()
			if err != nil {
				if own.conn.IsClosed() {
					slog.Info("close connection to registrar.", "identity", proto.Identity.String(), "addr", string(proto.Host))
				} else {
					slog.Error("gateway received error: "+err.Error(), "identity", proto.Identity.String(), "addr", string(proto.Host))
				}
				break
			}
			slog.Info(proto.String())

			// 处理数据
			if handler, ok := own.handlers[proto.Command]; ok {
				handler(proto, own.conn)
			}

			// 如果收到注册中心的注销指令，则退出当前连接
			if proto.Command == protocol.CMD_UNREGISTER {
				break
			}
		}

		// 如果本端主动断开连接，则直接退出程序
		if own.conn.IsClosed() {
			break
		}
	}
}
