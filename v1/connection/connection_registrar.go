package connection

import (
	"net"

	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

type RegistrarConn struct {
	*connection
}

func NewRegistrarConn(conn net.Conn, heartbeatInterval int) *RegistrarConn {
	return &RegistrarConn{
		connection: newConn(conn, protocol.IDE_REGISTRAR, protocol.IDE_REGISTRAR),
	}
}
