package connection

import (
	"net"
	"sync"

	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

type CommandHandler func(proto *protocol.Procotol, conn Conn)

type Conn interface {
	// 认证连接
	Auth(pwd string) error
	// 启动心跳
	WithHeart(itv int)
	// 注销连接
	Unregister() error

	// 接收协议包
	Recv() (*protocol.Procotol, error)
	// 发送协议包
	Send(cmd protocol.Command, data any) error

	// 检查连接是否关闭
	IsClosed() bool
	// 获取连接的元数据
	GetMetadata() *metadata
	// 获取最后发送的数据包
	GetLastSendPacket() *protocol.Procotol
	// 获取最后接收的数据包
	GetLastRecvPacket() *protocol.Procotol
}

var (
	_ Conn = &GatewayConn{}
	_ Conn = &InstanceConn{}
	_ Conn = &RegistrarConn{}
)

type connection struct {
	isClosed bool      // 连接是否关闭
	Meta     *metadata // 元信息
	heart    *heart    // 心跳
	conn     net.Conn  // 原始连接句柄
	lock     sync.RWMutex

	lastRecvPacket *protocol.Procotol // 最后接收的数据包
	lastSendPacket *protocol.Procotol // 最后发送的数据包
}

func newConn(conn net.Conn, localIdentity, remoteIdentity protocol.Identity) *connection {
	return &connection{
		Meta: newMetadata(conn, localIdentity, remoteIdentity),
		conn: conn,
	}
}

func (own *connection) WithHeart(itv int) {
	own.heart = newHeart(own, itv)
}

func (own *connection) Auth(pwd string) (err error) {
	own.lastRecvPacket, err = RequestAuth(own.conn, own.Meta.LocalIdentity, pwd)
	if err != nil {
		own.close()
	}

	return
}

func (own *connection) Recv() (*protocol.Procotol, error) {
	own.lock.Lock()
	proto, err := protocol.Read(own.conn)
	own.lock.Unlock()

	own.lastRecvPacket = proto
	return proto, err
}

func (own *connection) Send(cmd protocol.Command, data any) error {
	own.lock.Lock()
	proto, err := protocol.NewPolarisProtocol(cmd, own.Meta.LocalIdentity, own.Meta.LocalAddr, data)
	if err != nil {
		own.lock.Unlock()
		return err
	}

	if err = proto.Write(own.conn); err != nil {
		own.lock.Unlock()
		return err
	}

	own.lastSendPacket = proto
	own.lock.Unlock()
	return nil
}

func (own *connection) Unregister() error {
	if err := own.Send(protocol.CMD_UNREGISTER, nil); err != nil {
		return err
	}

	return own.close()
}

func (own *connection) GetMetadata() *metadata {
	return own.Meta
}

func (own *connection) GetLastSendPacket() *protocol.Procotol {
	return own.lastRecvPacket
}

func (own *connection) GetLastRecvPacket() *protocol.Procotol {
	return own.lastRecvPacket
}

func (own *connection) IsClosed() bool {
	own.lock.RLock()
	res := own.isClosed
	own.lock.RUnlock()
	return res
}

func (own *connection) close() error {
	own.lock.Lock()
	if own.heart != nil {
		own.heart.isStopped = true
	}

	own.isClosed = true
	err := own.conn.Close()
	own.lock.Unlock()

	return err
}
