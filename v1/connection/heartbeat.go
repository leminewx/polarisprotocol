package connection

import (
	"log/slog"
	"time"

	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

type heart struct {
	isStopped    bool          // 心跳停止标志
	beatInterval time.Duration // 心跳间隔时长
}

func newHeart(conn *connection, beatInterval int) *heart {
	h := &heart{
		beatInterval: time.Second * time.Duration(beatInterval),
	}
	go h.beat(conn)
	return h
}

func (own *heart) beat(conn *connection) {
	ticker := time.NewTicker(own.beatInterval)
	for range ticker.C {
		if err := conn.Send(protocol.CMD_HEARTBEAT, nil); err != nil {
			if own.isStopped {
				slog.Info("heartbeat stop.", "identity", conn.Meta.RemoteIdentity.String(), "addr", conn.Meta.RemoteAddr)
			} else {
				slog.Error("heartbeat interrupt: "+err.Error(), "identity", conn.Meta.RemoteIdentity.String(), "addr", conn.Meta.RemoteAddr)
			}
			return
		}

		slog.Info("heartbeat ok.", "identity", conn.Meta.RemoteIdentity.String(), "addr", conn.Meta.RemoteAddr)
		ticker.Reset(own.beatInterval)
	}
}
