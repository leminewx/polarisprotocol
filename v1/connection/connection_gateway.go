package connection

import (
	"net"

	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

type GatewayConn struct {
	*connection
}

func NewGatewayConn(conn net.Conn, heartbeatInterval int) *GatewayConn {
	c := &GatewayConn{
		connection: newConn(conn, protocol.IDE_GATEWAY, protocol.IDE_REGISTRAR),
	}

	c.WithHeart(heartbeatInterval)
	return c
}
