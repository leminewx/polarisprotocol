package connection

import (
	"net"
	"strings"

	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

type metadata struct {
	LocalIp        string            // 本端IP
	RemoteIp       string            // 对端IP
	LocalAddr      string            // 本端地址
	RemoteAddr     string            // 对端地址
	LocalIdentity  protocol.Identity // 本端身份
	RemoteIdentity protocol.Identity // 对端身份
}

func newMetadata(conn net.Conn, localIdentity, remoteIdentity protocol.Identity) *metadata {
	return &metadata{
		LocalIp:        strings.Split(conn.LocalAddr().String(), ":")[0],
		RemoteIp:       strings.Split(conn.RemoteAddr().String(), ":")[0],
		LocalAddr:      conn.LocalAddr().String(),
		RemoteAddr:     conn.RemoteAddr().String(),
		LocalIdentity:  localIdentity,
		RemoteIdentity: remoteIdentity,
	}
}
