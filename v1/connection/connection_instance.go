package connection

import (
	"net"

	"gitee.com/leminewx/polarisprotocol/v1/model"
	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

type InstanceConn struct {
	*connection
}

func NewInstanceConn(conn net.Conn, heartbeatInterval int) *InstanceConn {
	c := &InstanceConn{
		connection: newConn(conn, protocol.IDE_INSTANCE, protocol.IDE_REGISTRAR),
	}

	c.WithHeart(heartbeatInterval)
	return c
}

func (own *InstanceConn) Register(ins *model.Instance) error {
	return own.Send(protocol.CMD_REGISTER, ins)
}
