package connection

import (
	"errors"
	"fmt"
	"log/slog"
	"net"

	"gitee.com/leminewx/polarisprotocol/v1/protocol"
)

// RequestAuth 请求认证
func RequestAuth(conn net.Conn, identity protocol.Identity, pwd string) (*protocol.Procotol, error) {
	var err error
	var proto *protocol.Procotol

	// 向对端发起认证
	if err = protocol.Write(conn, protocol.CMD_AUTH, identity, conn.LocalAddr().String(), map[string]string{"pwd": pwd}); err != nil {
		return nil, err
	}

	// 等待对端认证结果
	proto, err = protocol.Read(conn)
	slog.Info(proto.String())
	if err != nil {
		return proto, err
	} else if proto.Command != protocol.CMD_OK { // 认证不成功
		return proto, fmt.Errorf("auth failed: %s", proto.Data)
	} else if proto.Identity != protocol.IDE_REGISTRAR { // 只能注册到中心注册
		return proto, errors.New("auth failed: remote is not registrar")
	}

	slog.Info("registered successfully.", "identity", proto.Identity.String(), "addr", string(proto.Host))
	return proto, nil
}

// ResponseAuth 响应认证
func ResponseAuth(conn net.Conn, identity protocol.Identity, pwd string) (*protocol.Procotol, error) {
	var err error
	var proto *protocol.Procotol

	// 等待对端认证
	data := map[string]string{}
	proto, err = protocol.Read(conn)
	fmt.Println(proto.String())
	if err != nil {
		protocol.Write(conn, protocol.CMD_FAIL, identity, conn.LocalAddr().String(), err.Error())
		return proto, err
	} else if proto.Command != protocol.CMD_AUTH {
		protocol.Write(conn, protocol.CMD_FAIL, identity, conn.LocalAddr().String(), "non authorized command")
		return proto, errors.New("non authorized command")
	} else if err = proto.ParseBody(&pwd); err != nil || data["pwd"] != pwd {
		protocol.Write(conn, protocol.CMD_FAIL, identity, conn.LocalAddr().String(), "authorized password error")
		return proto, fmt.Errorf("authorized password error: %s", proto.Data)
	}

	// 对端认证成功
	if err = protocol.Write(conn, protocol.CMD_OK, identity, conn.LocalAddr().String(), nil); err != nil {
		return nil, err
	}

	slog.Info("registered to local registrar successfully.", "identity", proto.Identity.String(), "addr", string(proto.Host))
	return proto, nil
}
