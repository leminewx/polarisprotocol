package protocol

import (
	"bytes"
	"testing"

	"gitee.com/leminewx/polarisprotocol/v1/model"
)

func TestProtocol_Auth(t *testing.T) {
	buffer := new(bytes.Buffer)
	if err := Write(buffer, CMD_AUTH, IDE_INSTANCE, "1.1.1.1:9999", map[string]any{
		"pwd": "123qwe!@#",
	}); err != nil {
		t.Fatal(err)
	}

	proto, err := Read(buffer)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(proto.String())

	pwd := map[string]string{}
	if err := proto.ParseBody(&pwd); err != nil {
		t.Fatal(err)
	}
	t.Logf("%v\n", pwd)
}

func TestProtocol_Heartbeat(t *testing.T) {
	buffer := new(bytes.Buffer)
	if err := Write(buffer, CMD_HEARTBEAT, IDE_INSTANCE, "1.1.1.1:9999", nil); err != nil {
		t.Fatal(err)
	}

	proto, err := Read(buffer)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(proto.String())
}

func TestProtocol_Register(t *testing.T) {
	buffer := new(bytes.Buffer)
	if err := Write(buffer, CMD_HEARTBEAT, IDE_INSTANCE, "1.1.1.1:9999", &model.Instance{
		Addr:        "172.22.38.20:20771",
		Name:        "user-a",
		Protocol:    model.INS_PROTO_HTTP,
		Namespace:   "test",
		ServiceName: "user",
		ServiceVer:  "v1",
	}); err != nil {
		t.Fatal(err)
	}

	proto, err := Read(buffer)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(proto.String())

	ins := &model.Instance{}
	if err := proto.ParseBody(&ins); err != nil {
		t.Fatal(err)
	}
	t.Logf("%v\n", ins)
}
