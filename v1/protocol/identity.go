package protocol

type Identity uint8

const (
	IDE_GATEWAY Identity = iota
	IDE_INSTANCE
	IDE_REGISTRAR
)

func (own Identity) String() string {
	switch own {
	case IDE_GATEWAY:
		return "gateway"
	case IDE_INSTANCE:
		return "instance"
	case IDE_REGISTRAR:
		return "registrar"
	default:
		return "unknown"
	}
}

func HasIdentity(sender Identity) bool {
	switch sender {
	case IDE_GATEWAY, IDE_INSTANCE, IDE_REGISTRAR:
		return true
	default:
		return false
	}
}
