package protocol

import (
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"time"
)

var (
	Magic   = [6]byte{2, 0, 7, 7, 1, 3}
	Version = [2]byte{'V', '1'}
)

type Procotol struct {
	Version    [2]byte  // 协议版本: V1
	Command    Command  // 请求方指令
	Identity   Identity // 请求方身份: 0-Gateway; 1-Instance; 2-Registrar
	Timestamp  int64    // 时间戳
	HostLength int16    // 主机长度
	Host       []byte   // 主机
	DataLength int16    // 数据体长度
	Data       []byte   // 数据体：JSON
}

func NewPolarisProtocol(cmd Command, identity Identity, host string, data any) (*Procotol, error) {
	proto := &Procotol{
		Version:    Version,
		Command:    cmd,
		Identity:   identity,
		Timestamp:  time.Now().UnixNano(),
		HostLength: int16(len(host)),
		Host:       []byte(host),
	}

	// 处理数据体
	if data != nil {
		var err error
		if proto.Data, err = json.Marshal(data); err != nil {
			return nil, err
		}
		proto.DataLength = int16(len(proto.Data))
	}

	return proto, nil
}

// Write 将协议数据写入指定的writer
func (own *Procotol) Write(writer io.Writer) error {
	if err := binary.Write(writer, binary.BigEndian, &Magic); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, &Version); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, &own.Command); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, &own.Identity); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, &own.Timestamp); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, &own.HostLength); err != nil {
		return err
	}
	if err := binary.Write(writer, binary.BigEndian, &own.Host); err != nil {
		return err
	}

	if err := binary.Write(writer, binary.BigEndian, &own.DataLength); err != nil {
		return err
	}
	if own.DataLength != 0 {
		return binary.Write(writer, binary.BigEndian, &own.Data)
	}
	return nil
}

// Read 从指定reader中读取协议数据
func (own *Procotol) Read(reader io.Reader) error {
	// 读取并校验幻数
	if err := own.readMagic(reader); err != nil {
		return err
	}

	// 读取协议数据
	if err := binary.Read(reader, binary.BigEndian, &own.Version); err != nil {
		return err
	}
	if err := binary.Read(reader, binary.BigEndian, &own.Command); err != nil {
		return err
	}
	if err := binary.Read(reader, binary.BigEndian, &own.Identity); err != nil {
		return err
	}
	if err := binary.Read(reader, binary.BigEndian, &own.Timestamp); err != nil {
		return err
	}

	if err := binary.Read(reader, binary.BigEndian, &own.HostLength); err != nil {
		return err
	}
	own.Host = make([]byte, own.HostLength)
	if err := binary.Read(reader, binary.BigEndian, &own.Host); err != nil {
		return err
	}

	if err := binary.Read(reader, binary.BigEndian, &own.DataLength); err != nil {
		return err
	}
	if own.DataLength != 0 {
		own.Data = make([]byte, own.DataLength)
		if err := binary.Read(reader, binary.BigEndian, &own.Data); err != nil {
			return err
		}
	}

	return own.Validate()
}

func (own *Procotol) readMagic(reader io.Reader) error {
	magic := make([]byte, 6)
	if _, err := reader.Read(magic); err != nil {
		return err
	} else if IsValidMagic(magic) {
		return fmt.Errorf("invalid magic: %d", magic)
	}
	return nil
}

// IsValidMagic 检查幻数是否有效
func IsValidMagic(magic []byte) bool {
	return magic[0] != Magic[0] || magic[1] != Magic[1] || magic[2] != Magic[2] || magic[3] != Magic[3] || magic[4] != Magic[4] || magic[5] != Magic[5]
}

// Validate 验证协议是否有效
func (own *Procotol) Validate() error {
	if own.Version != Version {
		return fmt.Errorf("invalid version: %s", own.Version)
	} else if !HasCommand(own.Command) {
		return fmt.Errorf("invalid command: %d", own.Command)
	} else if !HasIdentity(own.Identity) {
		return fmt.Errorf("invalid identity: %d", own.Identity)
	}
	return nil
}

// ParseBody 解析数据体
func (own *Procotol) ParseBody(data any) error {
	if own.Data == nil {
		return errors.New("no body data")
	}

	return json.Unmarshal(own.Data, data)
}

func (own *Procotol) String() string {
	loc, _ := time.LoadLocation("Asia/Shanghai")
	return fmt.Sprintf("PolarisProtocolData ===> {\n"+
		"\tVersion:    %s\n"+
		"\tCommand:    %s\n"+
		"\tIdentity:   %s\n"+
		"\tTimestamp:  %s\n"+
		"\tHostLength: %d\n"+
		"\tHost:       %s\n"+
		"\tDataLength: %d\n"+
		"\tData:       %s\n}\n", own.Version, own.Command.String(), own.Identity.String(),
		time.Unix(0, own.Timestamp).In(loc).Format("2006-01-02 15:04:05.000"), own.HostLength, own.Host, own.DataLength, own.Data,
	)
}

func Write(writer io.Writer, cmd Command, identity Identity, host string, data any) error {
	proto, err := NewPolarisProtocol(cmd, identity, host, data)
	if err != nil {
		return err
	}
	return proto.Write(writer)
}

func Read(reader io.Reader) (*Procotol, error) {
	proto := &Procotol{}
	return proto, proto.Read(reader)
}
