package protocol

// Command 定义请求命令的数据类型
type Command uint16

const (
	// 公共指令
	CMD_OK   Command = iota // 请求成功，可有可无
	CMD_FAIL                // 请求失败，需要数据

	CMD_AUTH       // 认证，需要数据
	CMD_REGISTER   // 注册，需要数据
	CMD_HEARTBEAT  // 心跳，无需数据
	CMD_UNREGISTER // 注销，无需数据

	// 同步指令
	CMD_SYNC_REG // 同步注册中心的数据，需要数据（通常新的客户端注册到注册中心后会执行该指令）

	// 通知指令
	CMD_NOTIFY_REGISTER_INS  // 注册中心广播通知注册应用实例，需要数据（应用实例注册后，注册中心节点需要广播该应用实例）
	CMD_NOTIFY_REGISTER_REG  // 注册中心广播通知注册注册中心，需要数据（注册中心注册后，注册中心节点需要广播该注册中心）
	CMD_NOTIFY_REGISTER_GATE // 注册中心广播通知注册网关平台，需要数据（网关平台注册后，注册中心节点需要广播该网关平台）

	CMD_NOTIFY_UNREGISTER_INS  // 注册中心广播通知注销应用实例，需要数据
	CMD_NOTIFY_UNREGISTER_REG  // 注册中心广播通知注销注册中心，需要数据
	CMD_NOTIFY_UNREGISTER_GATE // 注册中心广播通知注销网关平台，需要数据
)

func (own Command) String() string {
	switch own {
	case CMD_OK:
		return "ok"
	case CMD_FAIL:
		return "fail"
	case CMD_AUTH:
		return "auth"
	case CMD_REGISTER:
		return "register"
	case CMD_HEARTBEAT:
		return "heartbeat"
	case CMD_UNREGISTER:
		return "unregister"
	case CMD_SYNC_REG:
		return "sync registrar"
	case CMD_NOTIFY_REGISTER_INS:
		return "boradcast register instance"
	case CMD_NOTIFY_REGISTER_REG:
		return "boradcast register registrar"
	case CMD_NOTIFY_REGISTER_GATE:
		return "boradcast register gateway"
	case CMD_NOTIFY_UNREGISTER_INS:
		return "boradcast unregister instance"
	case CMD_NOTIFY_UNREGISTER_REG:
		return "boradcast unregister registrar"
	case CMD_NOTIFY_UNREGISTER_GATE:
		return "boradcast unregister gateway"
	default:
		return "unknown command"
	}
}

func HasCommand(cmd Command) bool {
	switch cmd {
	case CMD_OK, CMD_FAIL, CMD_AUTH, CMD_REGISTER, CMD_HEARTBEAT, CMD_UNREGISTER, CMD_SYNC_REG, CMD_NOTIFY_REGISTER_INS, CMD_NOTIFY_REGISTER_REG, CMD_NOTIFY_REGISTER_GATE,
		CMD_NOTIFY_UNREGISTER_INS, CMD_NOTIFY_UNREGISTER_REG, CMD_NOTIFY_UNREGISTER_GATE:
		return true
	default:
		return false
	}
}
