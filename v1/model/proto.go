package model

type InstanceProtocol uint8

const (
	INS_PROTO_RPC       InstanceProtocol = iota // RPC
	INS_PROTO_HTTP                              // HTTP
	INS_PROTO_WEBSOCKET                         // Websocket
)

func (own InstanceProtocol) String() string {
	switch own {
	case INS_PROTO_RPC:
		return "RPC"
	case INS_PROTO_HTTP:
		return "HTTP"
	case INS_PROTO_WEBSOCKET:
		return "Websocket"
	default:
		return "Unknown"
	}
}

func HasInstanceProtocol(insProto InstanceProtocol) bool {
	switch insProto {
	case INS_PROTO_RPC, INS_PROTO_HTTP, INS_PROTO_WEBSOCKET:
		return true
	default:
		return false
	}
}
