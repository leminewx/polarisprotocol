package model

import (
	"fmt"
	"strings"
)

// Registrar 定义注册中心的数据结构
type Registrar struct {
	Gateways   []string                   `json:"gateways"`   // 当前注册中心连接的网关平台数量
	Instances  map[string]*Instance       `json:"instances"`  // 当前注册中心已注册的应用实例
	Registrars map[string]*OtherRegistrar `json:"registrars"` // 当前注册中心同步的其他注册中心
}

func NewRegistrar() *Registrar {
	return &Registrar{
		Gateways:   make([]string, 0, 30),
		Instances:  make(map[string]*Instance),
		Registrars: make(map[string]*OtherRegistrar),
	}
}

// --------------注册--------------

func (own *Registrar) RegisterGateway(addr string) error {
	for _, gate := range own.Gateways {
		if gate == addr {
			return fmt.Errorf("already exist gateway: %s", addr)
		}
	}
	own.Gateways = append(own.Gateways, addr)
	return nil
}

func (own *Registrar) RegisterInstance(cid string, ins *Instance) error {
	if _, ok := own.Instances[cid]; ok {
		return fmt.Errorf("already exist instance: %s", cid)
	}

	own.Instances[cid] = ins
	return nil
}

func (own *Registrar) RegisterRegistrarGateway(rcid, gateAddr string) error {
	reg, ok := own.Registrars[rcid]
	if !ok {
		return fmt.Errorf("not found registrar: %s", rcid)
	}

	for _, reg := range reg.Gateways {
		if reg == gateAddr {
			return fmt.Errorf("already exist gateway: %s - %s", rcid, gateAddr)
		}
	}

	reg.Gateways = append(reg.Gateways, gateAddr)
	return nil
}

func (own *Registrar) RegisterRegistrarInstance(rcid, icid string, ins *Instance) error {
	reg, ok := own.Registrars[rcid]
	if !ok {
		reg = NewOtherRegistrar()
		own.Registrars[rcid] = reg
	}

	if _, ok := own.Instances[icid]; ok {
		return fmt.Errorf("already exist instance: %s - %s", rcid, icid)
	}

	reg.Instances[icid] = ins
	return nil
}

// --------------注销--------------

func (own *Registrar) UnregisterGateway(cid string) (string, error) {
	for idx, gate := range own.Gateways {
		if strings.HasPrefix(gate, cid) {
			own.Gateways = append(own.Gateways[:idx], own.Gateways[idx+1:]...)
			return gate, nil
		}
	}
	return "", fmt.Errorf("not found gateway: %s", cid)
}

func (own *Registrar) UnregisterInstance(cid string) (*Instance, error) {
	fmt.Printf("%v\n", own.Instances)
	if ins, ok := own.Instances[cid]; ok {
		delete(own.Instances, cid)
		return ins, nil
	}

	return nil, fmt.Errorf("not found instance: %s", cid)
}

func (own *Registrar) UnregisterRegistrar(cid string) error {
	if _, ok := own.Registrars[cid]; ok {
		delete(own.Registrars, cid)
		return nil
	}

	return fmt.Errorf("not found registrar: %s", cid)
}

func (own *Registrar) UnregisterRegistrarGateway(rcid, gateAddr string) error {
	reg, ok := own.Registrars[rcid]
	if !ok {
		return fmt.Errorf("not found registrar: %s", rcid)
	}

	for idx, addr := range reg.Gateways {
		if addr == gateAddr {
			reg.Gateways = append(reg.Gateways[:idx], reg.Gateways[idx+1:]...)
			return nil
		}
	}

	return fmt.Errorf("not found gateway: %s - %s", rcid, gateAddr)
}

func (own *Registrar) UnregisterRegistrarInstance(rcid, icid string) error {
	reg, ok := own.Registrars[rcid]
	if !ok {
		return fmt.Errorf("not found registrar: %s", rcid)
	}

	if _, ok := reg.Instances[icid]; ok {
		delete(reg.Instances, icid)
		return nil
	}

	return fmt.Errorf("not found instance: %s - %s", rcid, icid)
}

// --------------同步--------------

func (own *Registrar) SyncRegistrar(cid string, otherRegistrar *OtherRegistrar) {
	own.Registrars[cid] = otherRegistrar
}

// --------------同步--------------

func (own *Registrar) FetchGateway(cid string) string {
	for _, g := range own.Gateways {
		if strings.HasPrefix(g, cid) {
			return g
		}
	}
	return ""
}

// OtherRegistrar 定义其他注册中心节点的数据结构
type OtherRegistrar struct {
	Gateways  []string             `json:"gateways"`  // 其他注册中心连接的网关平台数量
	Instances map[string]*Instance `json:"instances"` // 其他注册中心已注册的应用实例
}

func NewOtherRegistrar() *OtherRegistrar {
	return &OtherRegistrar{
		Gateways:  make([]string, 0, 30),
		Instances: make(map[string]*Instance),
	}
}
