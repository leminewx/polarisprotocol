package model

// Instance 定义应用实例的数据结构
type Instance struct {
	Id          string           `json:"id"`           // 实例ID，可使用InstanceAddr
	Addr        string           `json:"addr"`         // 实例地址
	Name        string           `json:"name"`         // 实例名称
	Protocol    InstanceProtocol `json:"protocol"`     // 服务协议: 0-HTTP; 1-RPC; 3-Websocket;
	Namespace   string           `json:"namespace"`    // 命名空间
	ServiceName string           `json:"service_name"` // 服务名称
	ServiceVer  string           `json:"service_ver"`  // 服务版本
}

// RegistrarInstance
type RegistrarInstance struct {
	*Instance

	RegistrarAddr string `json:"registrar_addr"` // 实例所属注册中心地址
}
