注册应用实例
    1.应用实例1      注册用例     注册中心1                       InstanceRegisterRegistrar // 应用实例注册到注册中心
    2.注册中心1      广播通知     其他注册中心                       InstanceRegisterRegistrar
    3.注册中心       推送用例     网关平台                            InstanceRegisterGateway // 应用实例注册到网关平台

注册注册中心
    1.注册中心1     注册注册中心    注册中心2                      RegistrarRegisterRegistrar // 注册中心注册到注册中心
    2.注册中心2     广播通知        其他注册中心                 RegistrarRegisterRegistrar
    3.其他注册中心  连接到          注册中心1
    3.其他注册中心  推送用例        注册中心1                      RegistrarSyncRegistrar // 注册中心同步用例信息到注册中心

更新实例状态
    2.注册中心1      广播通知     其他注册中心                        InstanceUpdateRegistrar // 更新实例状态到注册中心
    3.注册中心       推送用例     网关平台                            InstanceUpdateGateway // 跟新实例状态到网关平台




网关平台注销实例
    1.1应用实例1属于网关直连注册中心1       注销实例        注册中心1 GatewayNotifyRegistrarUnregisterInstance // 网关通知注册中心注销实例
    1.2注册中心1                          广播通知        其他注册中心         RegistrarNotifyRegistrarUnregisterInstance// 注册中心通知注销实例
    1.3其他注册中心                       同步注销        直连网关平台          RegistrarNotifyGatewayUnregisterInstance // 注册中心通知网关注销实例
    
    2.1应用实例1不属于网关直连注册中心1     注销实例        注册中心1               GatewayNotifyRegistrarUnregisterInstance
    2.2注册中心1                          通知            应用实例1所属注册中心2    RegistrarNotifyRegistrarUnregisterInstance
    2.2注册中心1                          广播通知        其他注册中心              RegistrarNotifyRegistrarUnregisterInstance
    3.3其他注册中心                       同步注销        直连网关平台              RegistrarNotifyGatewayUnregisterInstance


注册中心注销实例
    1.应用实例          同步注销        直连网关平台         RegistrarNotifyRegistrarUnregisterInstance
    2.注册中心          广播通知        其他注册中心         RegistrarNotifyRegistrarUnregisterInstance
    3.其他注册中心       同步注销        直连网关平台          RegistrarNotifyGatewayUnregisterInstance

主动注销注册中心
    2.其他注册中心    连接到        直连网关平台        RegistrarNotifyGatewayUnregisterRegistrar




<!-- 实例注册 -->
1.实例注册到某一指定的注册中心；
2.注册中心与其建立连接后，将应用实例广播给其他注册中心；
3.连接网关平台的注册中心在将其注册到网关平台后，将应用实例广播给网关平台。

